import React, { Component } from "react";
import NewsCard from "../components/NewsCard";
import Loader from "@bit/semantic-org.semantic-ui-react.loader";
import { connect } from "react-redux";
import { fetchNews } from "../thunks/fetchNews";
import FetchButton from "../components/FetchButton";
import LongTextSnackbar from "../components/LongTextSnackbar";

import "./App.css";

class App extends Component {
  render() {
    const {
      fetchNews,
      fetchNewsSaga,
      isFetching,
      articles,
      error
    } = this.props;
    return (
      <div className="App">
        <header className="App-header">
          <div className="sticky">
            <FetchButton
              fetchDataThunk={fetchNews}
              fetchDataSaga={fetchNewsSaga}
            >
              Fetch NEWS
            </FetchButton>
          </div>
          <Loader active={isFetching} indeterminate>
            Preparing News
          </Loader>
          <div className="grid">
            {!isFetching &&
              !!articles &&
              articles.map((article, i) => {
                return article ? <NewsCard article={article} key={i} /> : null;
              })}
          </div>
          {!!error && <LongTextSnackbar message={error.message} />}
        </header>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isFetching: state.news.isFetching,
    articles: state.news.articles,
    error: state.news.error
  };
}
const fetchNewsSaga = () => dispatch => {
  dispatch({ type: "FETCH_NEWS_SAGA" });
};
export default connect(
  mapStateToProps,
  { fetchNews, fetchNewsSaga }
)(App);
