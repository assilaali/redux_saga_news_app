import { createStore, applyMiddleware } from "redux";
import combineReducers from "./reducers/index";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import createSagaMiddleware from 'redux-saga'
import watchFetchNewsSaga from './sagas/fetchNews'

//create logger Middleware
const loggerMiddleware = createLogger();
//init
const sagaMiddleware = createSagaMiddleware()
//middlewares
const middlewares = [thunk, sagaMiddleware, loggerMiddleware];
//create store 
const store = createStore(combineReducers, applyMiddleware(...middlewares));

sagaMiddleware.run(watchFetchNewsSaga)


export default store; 