import API from "../utils/API";
import {
  fetchNewsStarted,
  fetchNewsError,
  fetchNewsSuccess
} from "../actions/newsActions";

// fetching news starts now
export function fetchNews(subject = "bitcoin") {
  return dispatch => {
    dispatch(fetchNewsStarted());
    API
      .get(`${subject}`)
      .then(({ data }) => dispatch(fetchNewsSuccess({ data, subject })))
      .catch(err => dispatch(fetchNewsError(err)));
  };
}