import {
  FETCH_NEWS_START,
  FETCH_NEWS_SUCCESS,
  FETCH_NEWS_ERROR
} from "../actions/types/newsTypes";

const initialState = {
  articles: [],
  totalResults: 0,
  isFetching: false,
  error: null,
  subject: ''
};
const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_NEWS_START:
      return { ...state, isFetching: true };
    case FETCH_NEWS_SUCCESS:
      return {
        ...state,
        articles: action.payload.data.articles,
        totalResults: action.payload.data.totalResults,
        isFetching: false,
        subject: action.payload.subject
      };
    case FETCH_NEWS_ERROR:
      return { ...state, isFetching: false, error: action.payload };
    default:
      return state;
  }
};

export default newsReducer;
