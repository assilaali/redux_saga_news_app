import axios from "axios";

export default axios.create({
  baseURL: `https://newsapi.org/v2/everything?from=2019-07-29&sortBy=publishedAt&apiKey=3f42c9bbee0f4da2b9bb38a512c66086&q=`
});
